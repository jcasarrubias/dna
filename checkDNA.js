let countMutation = 0;

function checkDNA(combination) {
    const dna = combination;
    let dnaLength = dna.length;
    let obliqueLeft = '';
    let obliqueRight = '';
    for(var i = 0; i < dna.length; i++) {
        if(dna.length === dna[i].length){
            const str = dna[i];
            function repeatedCharHorizontal(str){
                const result = [];
                const strArr = str.toUpperCase().split("").sort().join("").match(/(.)\1+/g);
                
                if (strArr != null) {
                strArr.forEach((elem) => {
                    if(elem.length>3){
                        countMutation++;
                    }
                    result.push(elem[0]);
                });
                }
            }
            repeatedCharHorizontal(str);

            const strVertical = dna.map((dnas) => dnas[i]).join('');
            function repeatedCharVertical(strVertical){
                const result = [];
                const strArr = strVertical.toUpperCase().split("").sort().join("").match(/(.)\1+/g);
                
                if (strArr != null) {
                strArr.forEach((elem) => {
                    if(elem.length>3){
                        countMutation++;
                    }
                    result.push(elem[0]);
                });
                }
            }
            repeatedCharVertical(strVertical);

            var charObliqueRight = str.substring(i, i+1);
            obliqueRight += charObliqueRight;
            if(i === dna.length - 1) {
                const result = [];
                const strArr = obliqueRight.toUpperCase().split("").sort().join("").match(/(.)\1+/g);
                
                if (strArr != null) {
                strArr.forEach((elem) => {
                    if(elem.length>3){
                        countMutation++;
                    }
                    result.push(elem[0]);
                });
                }
            }

            var charObliqueLeft = str.substring(dnaLength, dnaLength-1);
            dnaLength--;
            obliqueLeft += charObliqueLeft;
            if(dnaLength === 0) {
                const result = [];
                const strArr = obliqueLeft.toUpperCase().split("").sort().join("").match(/(.)\1+/g);
                
                if (strArr != null) {
                strArr.forEach((elem) => {
                    if(elem.length>3){
                        countMutation++;
                    }
                    result.push(elem[0]);
                });
                }
            }
        } else {
            return false;
        }
    }

    if(countMutation > 1) {
        countMutation = 0;
        return true;
    } else {
        countMutation = 0;
        return false;
    }
}

module.exports = function hasMutation(combination) {
    if (combination.length < 4) return false;
    const filteredDNA = combination.filter((el) => (el.length === combination.length && /^[ATGC]+$/.test(el.toUpperCase())));
    if (filteredDNA.length !== combination.length) return false;
    return checkDNA(combination);
};
