const hasMutation = require('../checkDNA');

exports.checkMutation = (request, response) => {
  let isMutation;
  const { dna } = request.body;
  var client = request.app.get('connection');

  if (hasMutation(dna)) {
    response.status(200);
    response.send("El adn está mutado");
    isMutation=1;
  } else {
    response.status(403);
    response.send("El adn no está mutado");
    isMutation = 0;
  }
  var mutation = {'MUTATION':isMutation};
  client.query('INSERT INTO MUTATIONS set ?', mutation);
};

exports.getStats = (request, response) => {
  var client = request.app.get('connection');
  client.query('select (select count(MUTATION) from MUTATIONS where MUTATION = 0) as count_no_mutation, (select count(MUTATION) from MUTATIONS where MUTATION = 1) as count_mutation;',(err,rows) => {
    if(err) throw err;
    console.log('Data received from Db:');
    let count_mutation, count_no_mutation, ratio;
    console.log(rows);
    count_mutation = rows[0].count_mutation;
    count_no_mutation = rows[0].count_no_mutation;
    ratio = count_no_mutation !==0 ? count_mutation/count_no_mutation : 1;
    try {
      response.status(200).json({
        count_mutations: count_mutation,
        count_no_mutation: count_no_mutation,
        ratio: ratio,
      });
    } catch (e) {
        console.log("error");
    }
  });
};

