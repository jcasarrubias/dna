/**
 * Module dependencies.
 */ 

var express = require('express'),
  http = require('http'),
  bodyParser = require('body-parser'),
  async = require('async'),
  mysql = require('mysql');

const { checkMutation, getStats } = require('./controllers/mutation-controller');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.set('connection', mysql.createConnection({
    host: 'mutation.cwuklwre5j7g.us-east-2.rds.amazonaws.com',
    user: 'mutation',
    password: 'UZc8cabF3Jx4mCW',
    port: 3306}));
});

app.configure('production', function() {
  console.log('Using production settings.');
});

function init() {
  app.get('/', function(request, response){
    response.send("endpoint no valido");
  });
  app.post('/mutation', checkMutation);
  app.get('/stats', getStats);

  http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
  });
}

var client = app.get('connection');
async.series([
  function connect(callback) {
    client.connect(callback);
  },
  function clear(callback) {
    client.query('DROP DATABASE IF EXISTS mutation_db', callback);
  },
  function create_db(callback) {
    client.query('CREATE DATABASE mutation_db', callback);
  },
  function use_db(callback) {
    client.query('USE mutation_db', callback);
  },
  function create_table(callback) {
     client.query('CREATE TABLE MUTATIONS (' +
                         'ID bigint NOT NULL AUTO_INCREMENT, ' +
                         'MUTATION TINYINT(1), ' +
                         'REQUEST_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP, ' +
                         'PRIMARY KEY(ID))', callback);
  },
], function (err, results) {
  if (err) {
    console.log('Exception initializing database.');
    throw err;
  } else {
    console.log('Database initialization complete.');
    init();
  }
});
