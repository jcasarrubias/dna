# dna project

Para poder probar en local antes que nada se debe tener instalado node.

Una vez instalado Node se debe de clonar el proyecto:
```sh
$ git clone https://gitlab.com/jcasarrubias/dna.git
```

Después debes de acceder a la carpeta del proyecto:
```sh
$ cd dna
```

Ya dentro de la carpeta del proyecto se debe de hacer un npm install para instalar las dependencias:
```sh
$ npm install
```

Después se debe de inicializar el proyecto:
```sh
$ npm start
```

Para poder probar el endpoint `/mutation` se debe de utilizar una herramienta de testing de APIs:

Ejemplos de peticiones para `POST /mutation`
```sh
Mutado:
{
 "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}

No mutado:
{
 "dna":["ATGCGA", "CAGTGC", "TTGTGC", "AGAATG", "CCCTTA", "TCACTG"]
}

Con menos de 4 caracteres debe regresar no mutado:
{
  "dna":["ATC", "CAC", "TTT"]
}

A partir de los 4 caracteres ya se puede detectar una mutación:
{
  "dna":["ATGC", "CAGC", "TTTT", "CCCC"]
}

Si no es una matriz igual regresa no mutado:
{
 "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA"]
}

Si contiene caracteres no validos regresa no mutado:
{
 "dna":["ATGCGA", "CAGTGC", "TTATGS", "AGAAGD", "CCFCTA", "TCACTG"]
}
```

Para poder probar el endpoint `/stats` hay dos opciones, desde el navegador o desde la herramienta de testing de APIs:
      `GET /stats`

* Nota: En local la base de datos se borra/crea cada que se ejecuta la aplicación, así que para poder probar el `/stats` antes hay que hacer peticiones al `/mutation`.